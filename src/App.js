import React, {useState} from "react";
import Task from "./Task/Task";
import './styles/styles.css';

const App = () => {
    const [tasks, setTasks] = useState([
        {task: 'cook something', id: 1, completed: false},
        {task: 'buy car', id: 2, completed: false},
        {task: 'do homework', id: 3, completed: true},
        {task: 'wash hands', id: 4, completed: false}
    ]);

    const [inputValue, setInputValue] = useState('');

    const addTask = event => {
        if (event.key === 'Enter'){
            setTasks([
                ...tasks,
                {
                    id: Date.now(),
                    task:inputValue,
                    completed: false
                }
            ]);
            setInputValue('');
        }
    };

    const deleteTask = id => {
        setTasks(tasks.filter(task => task.id !== id));
    };

    const tasksList = tasks.map(task => (
        <Task
            key={task.id}
            {...task}
            id={task.id}
            onDelete={() => deleteTask(task.id)}
        >

        </Task>
    ))

    return (
        <div className="container p-4">
            <h4>ToDo</h4>
            <div className="input-group my-2">
                <input
                    className="form-control"
                    type="text"
                    placeholder="Write your task and press 'Enter' to add in ToDo-list"
                    value={inputValue}
                    onChange={event => setInputValue(event.target.value)}
                    onKeyPress={addTask}
                />
            </div>
            <ul className="p-0 my-2">
                {tasksList}
            </ul>
        </div>
    )
}
export default App;