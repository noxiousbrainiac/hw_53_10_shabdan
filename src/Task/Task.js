import React, {useState} from "react";
import '../styles/bootstrap.min.css';

const Task = props => {
    const [checked, setChecked] = useState(props.completed);
    const isDone = ['card m-2 d-flex flex-row p-2'];

    if (checked){
        isDone.push('isDone')
    }

    return (
        <li className={isDone.join(' ')} key={props.id}>
            <input
                className="checkBox"
                type="checkbox"
                checked={checked}
                onChange={() => setChecked(!checked)}
            />
            <span className="flex-grow-1 m-0 mx-2">{props.task}</span>
            <button
                className="btnTrash"
                onClick={props.onDelete}
            >
            </button>
        </li>
    )
};

export default Task;